import sys
from pdf import generate_cert, open_certificate
from sqlop import Database

"""
"Quiz game in Python"
Created on Sun Dec  4 09:59:31 2022
@author: Neha Rajpal
"""

total = 0
score = 0
isLoggedIn = False
isAdmin = False
name = None
user_id = None
db = Database()


def get_details():
    print("Please Login/Register to proceed:")
    print("1. Login")
    print("2. Sign up")
    print("3. Exit")
    choice = int(input("Enter your option "))
    if choice == 1:
        login()
    elif choice == 2:
        register()
        result = input("Do you want to continue? yes/no ")
        if result.lower() == 'yes':
            login()
        else:
            sys.exit()
    elif choice == 3:
        sys.exit()
    else:
        print("Invalid option please try again")


def register():
    username = input("Enter name ")
    userid = input("Enter userId ")
    password = input("Enter password ")
    is_admin = input("Are you Admin? (yes/no) ")
    bool_is_admin = False
    if is_admin.lower() == 'yes':
        bool_is_admin = True
    validate_registration(username, userid, password)
    sql = "INSERT INTO quiz.login (name, username, password, is_admin) VALUES (%s, %s, %s, %s)"
    values = (username, userid, password, bool_is_admin)
    db.add_record(sql, values)


def validate_registration(username, userid, password):
    if userid is None or userid.strip() == '':
        print("Invalid username")
        raise ValueError
    if username is None or username.strip() == '':
        print("Invalid name")
        raise ValueError
    if password is None or password.strip() == '':
        print("Invalid password")
        raise ValueError


def login():
    print("=====login=====")
    username = input("Enter username ")
    password = input("Enter password ")
    check_credentials(username, password)
    if isLoggedIn:
        global user_id
        user_id = username
        if isAdmin:
            show_admin_menu()
        else:
            show_quiz_menu()
    else:
        print("Invalid credentials")


def check_credentials(username, password):
    try:
        record = db.fetch_record("SELECT name, is_admin from quiz.login where username = %s and password =%s",
                                 (username, password))
        if record is not None:
            global name
            name = record[0]
            global isLoggedIn
            isLoggedIn = True
            if record[1] == 1:
                global isAdmin
                isAdmin = True
    except Exception as e:
        print("Invalid credentials")
        raise


def create_subjects():
    more = True
    while more:
        code = input("Enter Subject Code ")
        sub_name = input("Enter Subject Name ")
        validate_subject_data(code, sub_name)
        sql = "INSERT INTO quiz.subject (code, name) VALUES (%s, %s)"
        values = (code, sub_name)
        db.add_record(sql, values)
        result = input("Want to add more? yes/no ")
        if result.lower() != 'yes':
            more = False


def validate_subject_data(code, sub_name):
    if sub_name is None or sub_name.strip() == '':
        print("Invalid subject name")
        raise ValueError
    if code is None or code.strip() == '':
        print("Invalid code")
        raise ValueError


def create_questions():
    print("Available subject codes")
    pull_subjects()
    sub_code = input("Enter subject code ")
    more = True
    while more:
        question = input("Enter question ")
        option1 = input("Enter option 1 ")
        option2 = input("Enter option 2 ")
        option3 = input("Enter option 3 ")
        option4 = input("Enter option 4 ")
        answer = input("Enter correct answer ")
        validate_question_data(sub_code, question, option1, option2, option3, option4, answer)
        sql = "INSERT INTO quiz.questions (sub_code, question, option1, option2, option3, option4, answer) VALUES (%s, " \
              "%s, %s, %s, %s, %s, %s) "
        values = (sub_code, question, option1, option2, option3, option4, answer)
        db.add_record(sql, values)
        result = input("Want to add more? yes/no ")
        if result.lower() != 'yes':
            more = False


def is_valid_subject(code):
    try:
        records = db.fetch_record("SELECT code, name from quiz.subject where code=%s", (code,))
        if records is None:
            print("Invalid subject code")
    except Exception as e:
        print(f"Error fetching subject details {repr(e)}")
        raise


def validate_question_data(sub_code, question, option1, option2, option3, option4, answer):
    if question is None or question.strip() == '':
        print("Invalid question")
        raise ValueError
    if option1 is None or option1.strip() == '':
        print("Invalid option 1")
        raise ValueError
    if option2 is None or option2.strip() == '':
        print("Invalid option 2")
        raise ValueError
    if option3 is None or option3.strip() == '':
        print("Invalid option 3")
        raise ValueError
    if option4 is None or option4.strip() == '':
        print("Invalid option 4")
        raise ValueError
    if answer is None or answer.strip() == '':
        print("Invalid answer")
        raise ValueError
    is_valid_subject(sub_code)


def view_questions():
    try:
        records = db.fetch_all_records("SELECT sub_code, question from quiz.questions order by sub_code")
        if len(records) == 0:
            print("Questions not configured")
        else:
            for record in records:
                print(record)
    except Exception as e:
        print(f"Error fetching question details {repr(e)}")
        raise


def view_quiz_results():
    try:
        records = db.fetch_all_records("SELECT userId, username, score, total, resultDate from quiz.results")
        if len(records) == 0:
            print("Quiz not attempted yet")
        else:
            print("User ID\t", "Name\t", "Score\t", "Total\t", "Date")
            for record in records:
                print(record[0], "\t", record[1], "\t", record[2], "\t", record[3], "\t", record[4])
    except Exception as e:
        print(f"Error fetching result details {repr(e)}")
        raise


def show_admin_menu():
    print("Admin Menu:::")
    print("1. Create Subject Codes")
    print("2. Create Questions")
    print("3. View Subjects")
    print("4. View Questions")
    print("5. View Quiz Results")
    choice = int(input("Select your option "))
    if choice == 1:
        create_subjects()
    elif choice == 2:
        create_questions()
    elif choice == 3:
        pull_subjects()
    elif choice == 4:
        view_questions()
    elif choice == 5:
        view_quiz_results()
    else:
        print("Invalid option")
        sys.exit()


def pull_subjects():
    try:
        records = db.fetch_all_records("SELECT code, name from quiz.subject")
        if len(records) == 0:
            print("Subject codes not configured yet")
            sys.exit()
        else:
            for record in records:
                print(record)
    except Exception as e:
        print(f"Error fetching subject details {repr(e)}")
        raise


def start_quiz(sub_code):
    try:
        records = db.fetch_records("SELECT question, option1, option2, option3, option4, answer from quiz.questions "
                                   "where sub_code = %s", (sub_code,))
        global score
        score = 0
        global total
        total = 0
        if len(records) == 0:
            print("Quiz not configured.  Please visit after sometime")
            sys.exit()
        for record in records:
            total = total + 1
            print(record[0])
            print("1. ", record[1])
            print("2. ", record[2])
            print("3. ", record[3])
            print("4. ", record[4])
            ans = int(input("choose your answer by entering 1/2/3/4 "))
            if ans not in [1, 2, 3, 4]:
                print("Invalid option")
                score = score + 0
            elif record[ans] == record[5]:
                print("Correct answer!")
                score = score + 1
            else:
                print("You chose the wrong option :(")
                print(f"The correct answer is {record[5]}")
                score = score + 0
    except Exception as e:
        print(f"Error fetching questions {repr(e)}")
        raise


def show_quiz_menu():
    print("User Menu:::")
    print("Available subject codes")
    pull_subjects()
    choice = input("Enter subject code for which you want to attempt the quiz ")
    if choice is not None:
        start_quiz(choice)
        add_score()
        generate_cert(name.upper(), score, total)
        open_certificate()


def add_score():
    sql = "INSERT INTO quiz.results (userId, username, score, total) VALUES (%s, " \
          "%s, %s, %s) "
    values = (user_id, name, score, total)
    db.add_record(sql, values)


if __name__ == "__main__":
    get_details()

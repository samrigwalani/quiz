import jinja2
import pdfkit
from datetime import datetime

output_pdf = 'certificate.pdf'


def generate_cert(name, score, total):
    today_date = datetime.today().strftime("%d %b, %Y")
    context = {'name': name, 'score': score, 'today_date': today_date, 'total':total}
    template_loader = jinja2.FileSystemLoader('resources')
    template_env = jinja2.Environment(loader=template_loader)
    html_template = 'certificate.html'
    template = template_env.get_template(html_template)
    output_text = template.render(context)
    config = pdfkit.configuration(wkhtmltopdf='C:/Program Files/wkhtmltopdf/bin/wkhtmltopdf.exe')
    pdfkit.from_string(output_text, output_pdf, configuration=config, css='resources/style.css')


def open_certificate():
    import os
    os.system(output_pdf)
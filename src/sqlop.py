import mysql.connector
from mysql.connector import MySQLConnection


class Database(object):
    connection: MySQLConnection = None

    def __init__(self):
        cursor = None
        try:
            if not Database.connection:
                Database.connection = mysql.connector.connect(host="127.0.0.1", user="root", password="password")
            cursor = Database.connection.cursor()
            cursor.execute("CREATE DATABASE IF NOT EXISTS quiz")

            cursor.execute("CREATE TABLE IF NOT EXISTS quiz.login (name VARCHAR(50) not null, username VARCHAR("
                           "50) not null, "
                           "password VARCHAR(50) not null, is_admin bit(1) DEFAULT b'0', UNIQUE KEY "
                           "username_UNIQUE (username)); "
                           )
            cursor.execute(
                "CREATE TABLE IF NOT EXISTS quiz.subject (name varchar(50) not null, code varchar(50) not null, "
                "UNIQUE KEY "
                "subject_unique (name, code));")

            cursor.execute(
                "CREATE TABLE IF NOT EXISTS quiz.questions (question varchar(250) not null, sub_code varchar(50)"
                " not null, answer varchar(10) not null, option1 varchar(250) not null,"
                " option2 varchar(250) not null, option3 varchar(250) not null, "
                "option4 varchar(250) not null);")

            cursor.execute(
                "CREATE TABLE IF NOT EXISTS quiz.results (userId varchar(50) not null,"
                " username varchar(50) not null,"
                "score INT, total INT, resultDate date not null DEFAULT (current_date) );")

            print("db created")
        except mysql.connector.Error as err:
            print(f"mysql connection error {repr(err)}")
            raise
        finally:
            if cursor is not None:
                cursor.close()

    def add_record(self, sql, values):
        cursor = Database.connection.cursor()
        try:
            cursor.execute(sql, values)
            Database.connection.commit()
        except mysql.connector.Error as err:
            print(f"Couldn't perform insert to mysql {repr(err)}")
            Database.connection.rollback()
            raise
        finally:
            cursor.close()

    def fetch_record(self, sql, values):
        cursor = Database.connection.cursor()
        try:
            cursor.execute(sql, values)
            return cursor.fetchone()
        except mysql.connector.Error as err:
            print(f"Couldn't fetch record {repr(err)}")
            raise
        finally:
            cursor.close()

    def fetch_records(self, sql, values):
        cursor = Database.connection.cursor()
        try:
            cursor = Database.connection.cursor()
            cursor.execute(sql, values)
            return cursor.fetchall()
        except mysql.connector.Error as err:
            print(f"Couldn't fetch record {repr(err)}")
            raise
        finally:
            cursor.close()

    def fetch_all_records(self, sql):
        cursor = Database.connection.cursor()
        try:
            cursor = Database.connection.cursor()
            cursor.execute(sql)
            return cursor.fetchall()
        except mysql.connector.Error as err:
            print(f"Couldn't fetch record {repr(err)}")
            raise
        finally:
            cursor.close()

    def close_db(self):
        if Database.connection is not None:
            Database.connection.close()

@echo off
set "prog_path=%cd%"
:: Check for Python Installation
python --version 2>NUL
if errorlevel 1 goto errorNoPython

:: Check for wkhtmltopdf Installation
set "pathname=C:\Program Files\wkhtmltopdf\bin"
cd /d %pathname%
dir
.\wkhtmltopdf.exe --version 2>NUL
if errorlevel 1 goto errorNoWkhtmltopdf

cd /d %prog_path%
:: Install all dependencies
pip install -r ./requirements.txt

python src/main.py
:: Once done, exit the batch file -- skips executing the errorNoPython section
goto:eof

:errorNoPython
echo.
echo Error^: Python not installed

:errorNoWkhtmltopdf
echo.
echo Install wkhtmltopdf from https://wkhtmltopdf.org/downloads.html

:eof
pause